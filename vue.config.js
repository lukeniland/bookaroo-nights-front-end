process.env.VUE_APP_VERSION = require('./package.json').version

module.exports = {
  pwa: {
    name: "Bookaroo - Global",
    themeColor: "#00A4E4",
    msTileColor: "#000000",
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "black",

    // configure the workbox plugin
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      //swSrc: 'public/service-worker.js',
      swSrc: "src/service-worker.js"
      // ...other Workbox options...
    }
  },

  transpileDependencies: ["vuetify"]
};
