import Vue from "vue";
import Vuex from "vuex";
import Storage from "./services/storage";
import _ from 'lodash'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    depotId: '',
    // searchResults: [],
    // searchResultsCount: 0,
    scannedResponse: null,
    processedChosenResponse: null,
    chosenBarcodes: [],
    //numOfScanned: Storage.getNumberScanned(),
    scannedBarcodes: [],//Storage.getScannedCodes()
    listOfScans: []
  },
  getters: {

    chosenBarcodes: state => {
      //scannedResponse
      if (state.scannedResponse != null && state.scannedResponse.items != null) {
        let chosen = _.map(state.scannedResponse.items, function (x) {
          return {
            code: x.barCode,
            poNumber: x.poNumber,
            scannedAt: x.depotId,
            ipAddress: x.ipAddress
          }
        });
        return chosen;
      }
      return null
    },

    scannedBarcodes: state => {
      return state.listOfScans;
    },

    scannedBarcodesCount: state => {
      return state.listOfScans.length;
    },

    // numberOfScanned: state => {
    //   return state.scannedBarcodes
    // }

    // searchParams: state => {
    //   return {
    //     fromDate:state.fromDate,
    //     toDate:state.toDate,
    //     depotId:state.depotId,
    //   }
    // }
  },
  mutations: {
    // setSearchParams (state, model) {
    //   state.fromDate = model.fromDate
    //   state.toDate = model.toDate
    //   state.depotId = model.depotId
    // },
    setFromDate(state, fromDate) {
      state.fromDate = fromDate
    },
    setToDate(state, toDate) {
      state.toDate = toDate
    },
    setDepotId(state, depotId) {
      state.depotId = depotId
    },
    setSearchResults(state, results) {
      state.searchResults = results
    },
    setSearchResultsCount(state, count) {
      state.searchResultsCount = count
    },
    setScannedResponse(state, response) {
      state.scannedResponse = response
    },
    setProcessedChosenResponse(state, response) {
      state.processedChosenResponse = response
    },

    removeScannedCode(state, item) {

      let listWithRemoved = _.filter(state.scannedResponse.items, function (x) {
        return !(x.barCode == item.barCode && x.poNumber == item.poNumber && x.stockCode == item.stockCode);
      });
      state.scannedResponse.items = listWithRemoved
    },

    ///////////////////////////
    addBarcode(state, barcode) {
      Storage.saveScannedCode(barcode.toString());
      //let current = Storage.getScannedCodes()

      state.scannedBarcodes = Storage.getScannedCodes()//current
      //Vue.set(state.scannedBarcodes, current);
      //Vue.set(state.numberOfScanned, 25);

    },

    clear(state) {
      Storage.clear();
      //Vue.set(state.scannedBarcodes, []);
      //Vue.set(state.numberOfScanned, 0);
      let current = Storage.getScannedCodes()

      state.scannedBarcodes = current
      Vue.set(state.scannedBarcodes, current);
    },

    removeBarcode(state, barcode) {
     
      var index = state.scannedResponse.items.findIndex(b => b == barcode);
      state.scannedResponse.items.splice(index, 1)
      state.scannedResponse.numberOfItemsScanned = state.scannedResponse.numberOfItemsScanned - 1;
    },
    ///////////////////////////

  },
  actions: {}
});
