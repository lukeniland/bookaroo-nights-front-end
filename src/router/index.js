import Vue from "vue";
import VueRouter from "vue-router";

import ScanBespokeCode from "../views/ScanBespokeCode.vue";
import Splash from "../views/Splash.vue";
import ShowLast from "../views/ShowLast.vue";
import ProcessReceipt from "../views/ProcessReceipt.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/ScanBespokeCode",
    name: "ScanBespokeCode",
    component: ScanBespokeCode
  },
  {
    path: "/ProcessReceipt",
    name: "ProcessReceipt",
    component: ProcessReceipt
  },
  {
    path: "/Splash",
    name: "Splash",
    component: Splash
  },
  {
    path: "/ShowLast",
    name: "ShowLast",
    component: ShowLast
  },
  // {
  //   path: "/ScanManifestItems",
  //   name: "ScanManifestItems",
  //   component: ScanManifestItems
  // },
  // {
  //   path: "/ScanAdhocItems",
  //   name: "ScanAdhocItems",
  //   component: ScanAdhocItems
  // },
  
  {
    path: "/",
    name: "home",
    component: Splash
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;
