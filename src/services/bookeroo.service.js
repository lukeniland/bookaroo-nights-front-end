import axios from "axios";

export default {
  GetIpId(ipCheck) {
    return axios.get("/toCheck?ipCheck=" + ipCheck);
  },

  GetIdName(ipCheck) {
    return axios.get("/toCheckid?ipCheck=" + ipCheck);
  },

  getDepots() {
    return axios.get("/depots");
  },

  submitScannedBarcodes(model) {
    return axios.post('/submitBN', model);
    //return axios.post('/submitScannedBarcodes', model);
    //return this.submitScannedBarcodesDummyResponse()
  },

  processChosenBarcodes(depotId, version, model) {
    return axios.post("/processBN?depotId=" + depotId + "&version="+ version, model);
    //return axios.post('/processChosenBarcodes', model);
    //return this.processChosenBarcodesDummyResponse()
  }

};
