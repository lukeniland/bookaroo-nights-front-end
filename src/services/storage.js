/* eslint-disable */
const SCANNED_CODES = "scanneditems"

export const getScannedCodes = () => {
  let storageScannedItems = localStorage.getItem(SCANNED_CODES);
  console.log("These are in the variable storagescans: ", storageScannedItems);
  if (storageScannedItems) {
    let scannedItems = JSON.parse(storageScannedItems);
    console.log("after parse: ", scannedItems);
    return scannedItems
  }
  return []
}

export const saveScannedCode = code => {

  let scannedItems = [];
  let storageScannedItems = localStorage.getItem("scannedCodes");
  if (storageScannedItems) {
    scannedItems = JSON.parse(storageScannedItems);
  }
  var alreadyExists = false

  for (var i = 0; i < scannedItems.length; i++) {

    if (code.toLowerCase().endsWith(scannedItems[i].toLowerCase())) {
      alreadyExists = true;
    }
  }

  if (!alreadyExists) {
    //if (some(scannedItems,()=> {this.hiddenScanField}) == false) {
    console.log("saving " + code);
    scannedItems.push(code);
    localStorage.setItem("scannedCodes", JSON.stringify(scannedItems));
  } else {
    console.log("Already has " + code);
  }
}


export const clear = () => {
  window.localStorage.removeItem(SCANNED_CODES)
}

export default {
  getScannedCodes,
  saveScannedCode,
  clear,
}