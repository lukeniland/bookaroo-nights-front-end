import "@fortawesome/fontawesome-free/css/all.css";
import Vue from "vue";
import "./plugins/fontawesome";
import "./registerServiceWorker";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import "@/assets/scss/_global.scss";
import "vuetify/dist/vuetify.min.css";

import axios from 'axios';
axios.defaults.baseURL = process.env.VUE_APP_BASE_URL;

import TitleBar from "./components/TitleBar";
import "./registerServiceWorker";
Vue.component("TitleBar", TitleBar);

import IdleVue from 'idle-vue'
const eventsHub = new Vue()
 
Vue.use(IdleVue, {
  eventEmitter: eventsHub,
  idleTime: 1000
})

Vue.config.productionTip = false;

new Vue({
  el: "#app",
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
