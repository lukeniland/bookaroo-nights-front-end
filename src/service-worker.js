console.log(`Hello from service worker`);
if (workbox) {
  console.log(`Workbox is loaded`);
  console.log("yyy");

  self.__precacheManifest = [].concat(self.__precacheManifest || []);
  //workbox.precaching.suppressWarnings();
  workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

  /*   workbox.routing.registerRoute(
    process.env.VUE_APP_BASE_URL + "depots",
    new workbox.strategies.NetworkFirst()
  ); */

  // addEventListener('message', (event) => {
  //   if (event.data && event.data.type === 'SKIP_WAITING') {
  //     skipWaiting();
  //   }
  // });

  self.addEventListener("message", event => {
    if (event.data.action == "SKIP_WAITING") self.skipWaiting();
  });
} else {
  console.log(`Workbox didn't load`);
}
